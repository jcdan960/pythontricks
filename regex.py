#https://www.youtube.com/watch?v=sZyAn2TW7GY&ab_channel=sentdex
import re
'''
identifiers:
\d any number
\D anything but a number
\s space
\S anything but a space
\w any char
\W anything but a char
.  any char except for a newline
\b whitespace around words
\. a period

Modifiers:
{1,3} we're expecting 1-3 of them
{x} expecting x amount of them
? Match 0 or 1
* Match 0 or more
+ Match 1 or more 
$ Match the end of a string 
^ Match the beginning of a string
| either or
- range
[] or -> [A-Z]an will match Aan, Ban, Zan


White space characters:
\n new line
\s space 
\t tab 
\e escape
\f form feed 
\r carriage return

DONT FORGET:
need to escape the characters (e.g. $ -> need to do \$)

re Methods:
findall	Returns a list containing all matches
search	Returns a Match object if there is a match anywhere in the string
split	Returns a list where the string has been split at each match
sub	Replaces one or many matches with a string


re examples:
[arn]	Returns a match where one of the specified characters (a, r, or n) are present	
[a-n]	Returns a match for any lower case character, alphabetically between a and n	
[^arn]	Returns a match for any character EXCEPT a, r, and n	
[0123]	Returns a match where any of the specified digits (0, 1, 2, or 3) are present	
[0-9]	Returns a match for any digit between 0 and 9	
[0-5][0-9]	Returns a match for any two-digit numbers from 00 and 59	
[a-zA-Z]	Returns a match for any character alphabetically between a and z, lower case OR upper case	
[+]	In sets, +, *, ., |, (), $,{} has no special meaning, so [+] means: return a match for any + character in the string

https://www.w3schools.com/python/python_regex.asp



More notes:
Or:
^(Mr\.|Mrs\.)

starts by Mr. OR Mrs.

1 or + char
[a-zA-Z]+

0 or + char
[a-zA-Z]*

exactly 4 chars
[a-zA-Z]{4}

4 or more chars
[a-zA-Z]{4,}

grouping:

3 or 4 times the group, e.g. 45A 
([0-9]{2}[a-zA-Z]{1}){3,4}


this will work with ok AND ok:
[ok]{2}

because range [] does not care about order.

Need to do (ok){2}

'''

print('Starting to regex')

#basic fonctions:
string = 'Jessica is 15 years old, and Daniel is 27 years old. Edward is 97 and his grandfather, Oscar is 102.'

# need to be r string because regex
#1 to 3 digits
age_regex = r'\d{1,3}'
ages = re.findall(age_regex, string)
print(ages)

#first letter capital A to Z, second is 1 or more lower letter a-z
name_regex = r'[A-Z][a-z]*'

names = re.findall(name_regex, string)
print(names)