import re

def mail_is_valid(email_str) -> bool:
    if not email_str or type(email_str) != str:
        return False

    regex = r'^[0-9a-zA-z\-_]+@[a-zA-Z]+\.[a-z]{2,}'

    out = re.match(regex, email_str)

    return out is not None



answers = {None:False , 3: False, 'lara@hackerrank.com': True, 'brian-23@hackerrank.com': True, 'britts_54@hackerrank.com': True,
         '@gmail.com': False, 'xxx@jcjc.dsds': False, 'xxx@jcjc.dsds': True, '@@@': False}

for val, key in answers.items():
    if mail_is_valid(val) == key:
        print('nice')
    else:
        raise ValueError()

print('succcess')
