

def lengthOfLongestSubstring(s: str) -> int:
    last_idx = {}
    max_len = 0

    start_idx = 0

    for i in range(0, len(s)):
        current_char = s[i]
        if current_char in last_idx:
            start_idx = last_idx[current_char] + 1

        # Update result if we get a larger window
        max_len = max(max_len, i - start_idx + 1)

        # Update last index of current char.
        last_idx[current_char] = i

    return max_len

def is_next_different(idx, str):
    return str[idx] != str[idx+1]

if __name__ == "__main__":
    inputs = {"abca": 3, "bbbbb":1, "pwwkew":3}

    for key, value in inputs.items():
        assert lengthOfLongestSubstring(key) == value