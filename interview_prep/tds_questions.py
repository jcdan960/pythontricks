

def revere_int(int_as_str) -> int:
    int_as_str = str(int_as_str)

    if int(int_as_str) >= 0:
        return revese_int(int_as_str)
    else:
        return -1*revese_int(int_as_str[1:])


def revese_int(int_to_reverse) ->int:
    return int((int_to_reverse)[::-1])

print(revere_int(123))
print(revere_int(-123))



#Average Words Length

sentence1 = "Hi all, my name is Tom...I am originally from Australia."
sentence2 = "I need to work very hard to learn more about algorithms in Python!"

def average_lenght(phrase):
    for i in ',!?.;:':
        phrase = phrase.replace(i, '')

    phrase = phrase.split(' ')

    return sum([len(i) for i in phrase])/len(phrase)


print(average_lenght(sentence1))
print(average_lenght(sentence2))



# Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
# You must not use any built-in BigInteger library or convert the inputs to integer directly.

#Notes:
#Both num1 and num2 contains only digits 0-9.
#Both num1 and num2 does not contain any leading zero.

num1 = '364'
num2 = '1836'

def get_sum_of_str(num1, num2):
    return int(num1)+int(num2)

print(get_sum_of_str(num1,num2))

print('\n\n\n')

# Given a string, find the first non-repeating character in it and return its index.
# If it doesn't exist, return -1. # Note: all the input strings are already lowercase.

def first_non_repeating_char(str_cool):
    for i, val in enumerate(str_cool):
        copy = str_cool
        copy = copy.replace(val, '', 1)
        if val not in copy:
            return i


print(first_non_repeating_char('alphabet'))
print(first_non_repeating_char('barbados'))
print(first_non_repeating_char('crunchy'))


print('\n\n\n')

# Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.
# The string will only contain lowercase characters a-z.

def valid_palindrome(pal: str):
    if pal[::-1] == pal:
        return True

    for i in range(0, len(pal) -1):
        copy = pal
        copy = copy.replace(pal[i], '')
        if copy == copy[::-1]:
            return True

    return False

print(valid_palindrome('laval'))
print(valid_palindrome('radkar'))
print(valid_palindrome('raddhfdfuhkar'))


print('\n\n\n')

# Given an array of integers, determine whether the array is monotonic or not.
#An array is monotonic if and only if it is monotone increasing
A = [6, 5, 4, 4]
B = [1,1,1,3,3,4,3,2,4,2]
C = [1,1,2,3,7]


#the all() function that returns True if all items in an iterable are true, otherwise it returns False
def is_monotone(nums):
    return (all(nums[i] <= nums[i + 1] for i in range(len(nums) - 1)) or
            all(nums[i] >= nums[i + 1] for i in range(len(nums) - 1)))


#move zeros


#Given an array nums, write a function to move all zeroes to the end of it while maintaining the relative order of
#the non-zero elements.

array1 = [0,1,0,3,12]
array2 = [1,7,0,0,8,0,10,12,0,4]

def remove_all_zeros_to_the_end(arr : list) -> list:

    nb = arr.count(0)
    while(0 in arr):
        arr.remove(0)

    arr.extend([0 for i in range(0, nb)])

    return arr

print(remove_all_zeros_to_the_end(array1))

print(remove_all_zeros_to_the_end(array2))



print('\n\n\n\n')


# Given an array containing None values fill in the None values with most recent
# non None value in the array
array1 = [1,None,2,3,None,None,5,None]

def fill_the_blanc(arr):

    for i, val in enumerate(array1):
        if val is None:
            array1[i] = array1[i-1]

    return array1


print(fill_the_blanc(array1))


print('\n\n\n\n')
#9. Matched & Mismatched Words

#Given two sentences, return an array that has the words that appear in one sentence and not
#the other and an array with the words in common.
sentence1 = 'We are really pleased to meet you in our city'
sentence2 = 'The city was hit by a really heavy storm'

def solution(arr1, arr2):
    ret_array = []
    for i in arr1.split():
        if i not in arr2.split():
            ret_array.append(i)


    for i in arr2.split():
        if i not in arr1.split():
            ret_array.append(i)

    return ret_array



print(solution(sentence1, sentence2))


for i in range(0,35):
    print(i)


print('\n\n\n\n')
#Prime Numbers Array
n = 35

def cool(n):
    ret = []
    for i in range(2,n):
        if is_prime(i):
            ret.append(i)

    return ret

def is_prime(nb):
    for i in range(2, nb):
        if nb%i == 0:
            return False

    return True
print(cool(n))


print('\n\n\n\n')
