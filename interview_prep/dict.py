print("Dict")

cool_dict = {'apple': 'fruit', 'beetroot': 'vegetable', 'cake': 'dessert'}

#get value by index
val = cool_dict['apple']
print(val)

#get the values
all_vals = cool_dict.values()
print(all_vals)

#get the keys
all_keys = cool_dict.keys()
print(all_keys)

#get the items
all_items = cool_dict.items()
print(all_items)

#make a copy
second_dict = cool_dict.copy()


#remove my key
print(second_dict)
second_dict.pop('apple')
print(second_dict)

#dict comprehension
print('\n\nComprenhension')
# key:value
third_dict = {i:i for i in cool_dict}
print(third_dict)

original_dict = {'jack': 38, 'michael': 48, 'guido': 57, 'john': 33}
even_dict = {k: v for (k, v) in original_dict.items() if v % 2 == 0}

#EXOS:
#Exercise 1: Below are the two lists convert it into the dictionary
keys = ['Ten', 'Twenty', 'Thirty']
values = [10, 20, 30]

out = {val:key for (val,key) in zip(keys,values)}
print(out)


#Exercise 2: Merge following two Python dictionaries into one
dict1 = {'Ten': 10, 'Twenty': 20, 'Thirty': 30}
dict2 = {'Thirty': 30, 'Fourty': 40, 'Fifty': 50}

dict3 = {**dict1, **dict2}
print(dict3)


#Exercise 6: Delete set of keys from a dictionary

sampleDict = {
    "name": "Kelly",
    "age": 25,
    "salary": 8000,
    "city": "New york"
}

keysToRemove = ["name", "salary"]

for key in keysToRemove:
    del sampleDict[key]

print(sampleDict)


#Exercise 9: Get the key of a minimum value from the following dictionary

sampleDict = {
  'Physics': 82,
  'Math': 65,
  'history': 75
}


for key, val in sampleDict.items():
    if val == min(sampleDict.values()):
        print(key)



if 65 in sampleDict.values():
    print('yo')

