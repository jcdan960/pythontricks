class Solution:
    #m by n grid
    # can only move right or down
    def uniquePaths(self, m: int, n: int) -> int:

        row_under = [1 for i in range(n)]

        for i in range(m-1):
            new_row = [1 for i in range(n)]
            for j in range(n-2,-1,-1):
                new_row[j] = new_row[j+1] + row_under[j]
            row_under = new_row

        return new_row[0]

        '''

        if m > 0 and n > 0:
            return self.uniquePaths(m, n - 1) + self.uniquePaths(m, n - 1) + 2

        if m == 0:
            if n ==0:
                return 0
            else:
                return self.uniquePaths(m,n-1)

        if n ==0:
            return self.uniquePaths(m-1, n)

'''
sol = Solution()
m,n = 3,7

print(sol.uniquePaths(m,n))