class Solution:
    # @param A : tuple of integers
    # @return an integer
    def longestConsecutive(self, A):
        A = set(A)
        longuest = 1

        for i in A:
            if i-1 not in A:
                #this is the starting point of a subsequence
                j = i
                while (True):
                    if j+1 in A:
                        longuest = max(longuest, j-i + 2)
                        j = j +1
                    else:
                        break

        return longuest



A = [100, 4, 200, 1, 3, 2]
sol = Solution()
out = sol.longestConsecutive(A)

print(out)
