#https://dbader.org/blog/python-dunder-methods

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f'{self.name} : {self.age} year old'

    def __cmp__(self, other):
        return self.age == other.name

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return self.age < other.age

    def __len__(self):
        return 1

    def __reversed__(self):
        raise NotImplemented()

    def __getitem__(self, item):
        raise NotImplemented()

    def __call__(self):
        print("CALLING A METHOD FOR NO REASON")

JC = Person('JC Dansereau', 30)
print(JC)

jess = Person('Jessica', 31)

maggie = Person('Maggie la crepe', 1)

cool_list = [JC, jess, maggie]

print(cool_list)

cool_list.sort()

print(cool_list)

jess()