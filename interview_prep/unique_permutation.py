import itertools

class Solution:
    # @param A : list of integers
    # @return a list of list of integers
    def permute(self, A):
        arr = [A]

        for i in range(0, len(A) -1):
            A = self.permute_for_real(A)
            arr.append(A)

        return self.no_duplicate(arr)

    def no_duplicate(self, arr):
        return_arr = []

        for i in arr:
            if i not in return_arr:
                return_arr.append(i)

        return return_arr

    def permute_for_real(self, arr):
        return_arr = []
       # return_arr.append(arr[1])
        for i in range(0, len(arr) - 1):
            x = arr[i + 1]
            return_arr.append(x)

        return_arr.append(arr[0])
        return return_arr

arr = [ 1, 1 , 2 ]
sol =Solution()
print(sol.permute(arr))