class Solution:

    #input: 'babad'
    #output: 'bab
    def longestPalindrome(self, s: str) -> str:
        longuest = ''

        for i in range(len(s)):
            # even size palindrome
            left, right = i, i
            while left >= 0 and right < len(s) and s[left] == s[right]:
                if len(longuest) < right - left +1:
                    longuest = s[left:right + 1]
                left -= 1
                right += 1


            left, right = i, i+1
            while left >= 0 and right < len(s) and s[left] == s[right]:
                if len(longuest) < right - left +1:
                    longuest = s[left:right + 1]
                left -= 1
                right += 1

        # odd size palindrome
        return longuest






        '''
        #https://www.youtube.com/watch?v=XYQecbcd6_c&ab_channel=NeetCode
        res = ''
        resLen = 0

        for i in range(len(s)):
            #odd length
            l,r = i,i
            while(l >= 0 and r < len(s) and s[l] == s[r]):
                if (r-l+1) > resLen:
                    resLen = r-1+1
                    res = s[l:r+1]
                l -= 1
                r += 1



            #even length
            l, r = i, i+1

            while (l >= 0 and r < len(s) and s[l] == s[r]):
                if (r - l + 1) > resLen:
                    res = s[l:r + 1]
                    resLen = r -l +1
                l -= 1
                r += 1

        return res

    def longestPalindrome(self, s: str) -> str:
        if not s:
            return 0

        if len(s) <= 1:
            return len(s)

        if s == s[::-1]:
            return len(s)

        else:
            return self.longestPalindrome(s[:-1])
            '''




sol = Solution()
s = "babad"
longuest = sol.longestPalindrome(s)

print(longuest)