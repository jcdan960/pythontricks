def new_prob(prob_name):
    print(f'\n\nNEW PROBLEM : {prob_name.upper()}\n')


#print prime numbers between 100 and 200
new_prob('print between 100 and 200 prime numbers')

def print_prime_100_200():
    numbers = [i for i in range(100, 200)]
    for num in numbers:
        if is_prime(num):
            print(num)

def is_prime(number):
    for i in range(2, number):
        if number%i == 0:
            return False
    return True

print_prime_100_200()


new_prob('reverse string')

string = 'My Name is Jessa'

print(string)
print(string[::-1])


new_prob('reverse words')

reverse = ''

reversed = [word[::-1] for word in string.split(' ')]

print(' '.join(reversed))



new_prob('Remove items from a list while iterating')

number_list = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

#number_list.sort()

list_length = len(number_list)

current = 0
while(current < list_length):
    if number_list[current] > 50:
        del number_list[current]
        list_length -= 1

    else:
        current +=1


print(number_list)


new_prob('Reverse Dictionary mapping')

ascii_dict = {'A': 65, 'B': 66, 'C': 67, 'D': 68}

def reverse_dict_mapping(input_dict):
    output = {}

    for key, value in input_dict.items():
        output[value] = key

    return output



print(reverse_dict_mapping(ascii_dict))

new_prob('Display all duplicate items from a list')

sample_list = [10, 20, 60, 30, 20, 40, 30, 60, 70, 80]

def get_diff(input_list):
    duplicates = []
    for i in input_list:
        if input_list.count(i) > 1:
            duplicates.append(i)

    return set(duplicates)

print(get_diff(sample_list))


new_prob('Filter dictionary to contain keys present in the given list')

d1 = {'A': 65, 'B': 66, 'C': 67, 'D': 68, 'E': 69, 'F': 70}

# Filter dict using following keys
l1 = ['A', 'C', 'F']

d2 = d1.copy()
for key in d1.keys():
    if key not in l1:
        del d2[key]

print(d2)

new_prob('Print the following number pattern')

'''
1 1 1 1 1 
2 2 2 2 
3 3 3 
4 4 
5
'''


max_nb = 6
for i in range(1, max_nb):
    print(str(i)*(max_nb-i))



xxx = {"jc":30, 'jess':31}

print(xxx)

for key, val in xxx.items():
    xxx[val] = key

print(xxx)


#reversed for loop
for n in range(6, 0, -1):
    print(n)