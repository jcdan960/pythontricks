class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        if not l1 or not l2:
            return None

        l1 = self.reverse_ll(l1)
        l2 = self.reverse_ll(l2)

        val = str(l1 + l2)

        return self.make_linked_list(val)


    def make_linked_list(self, val : str) -> ListNode:
        node_list = []

        for i in reversed(val):
            node_list.append(ListNode(int(i)))

        first_node = node_list[0]
        for i in range(0, len(node_list) -1):
            node_list[i].next = node_list[i+1]

        return first_node


    def reverse_ll(self, lst):
        out = []
        current_node = lst
        while (current_node):
            out.append(current_node.val)
            current_node = current_node.next

        out.reverse()
        out = [str(i) for i in out]
        num = int(''.join(out))

        return num


a = ListNode(3, None)
b = ListNode(4,a)
f = ListNode(2,b)

c =ListNode(4, None)
d =ListNode(6,c)
g = ListNode(5,d)

l1 = [2,4,3]
l2 = [5,6,4]
sol = Solution()

sol.addTwoNumbers(f,g)