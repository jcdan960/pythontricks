my_tuple = (1,2,3,4,5,6,1)

print(my_tuple)

print(my_tuple.count(1))

print(my_tuple.index(2))


print('\n\n')
cool_set = {55, 72, 89}
other_set = {55, 72, 88, 66}

empty_set = set()

print(cool_set)
print(empty_set)
print(other_set)

# remove first item
print(other_set.pop())
print(other_set)

#remove by element
print(cool_set.remove(88))




