#A = [ 959, 329, 987, 951, 942, 410, 282, 376, 581, 507, 546, 299, 564, 114, 474, 163, 953, 481, 337, 395, 679, 21, 335, 846, 878, 961, 663, 413, 610, 937, 32, 831, 239, 899, 659, 718, 738, 7, 209 ]
#B = [ 862, 783, 134, 441, 177, 416, 329, 43, 997, 920, 289, 117, 573, 672, 574, 797, 512, 887, 571, 657, 420, 686, 411, 817, 185, 326, 891, 122, 496, 905, 910, 810, 226, 462, 759, 637, 517, 237, 884 ]

A = [ 1, 2, 3, 4, 5 ]
B = [ 3, 4, 5, 1, 2 ]

from typing import List
class Solution:
    # @param A : tuple of integers
    # @param B : tuple of integers
    # @return an integer

    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:

        #if true, we know there is a solution
        if sum(gas) < sum(cost):
            return -1

        res = total_gas = 0

        for i in range(len(gas)):
            total_gas += gas[i] - cost[i]

            if total_gas < 0:
                total_gas = 0
                res = i+1

        return res

sol = Solution()
out = sol.canCompleteCircuit(A, B)
print(out)

'''
    def canCompleteCircuit(self, A, B):

        cost_to_next_station = B.copy()
        gaz_at_current_station = A.copy()

        #edge cases
        if len(gaz_at_current_station) == 1:
            if cost_to_next_station[0] > gaz_at_current_station[0]:
                return -1
            else:
                return 0

        nb_gaz_station = len(gaz_at_current_station)

        for i in range(0, nb_gaz_station - 1):
            # next station to visit
            current_visiting_station = i

            #reset gaz to 0
            gaz_in_tank = 0

            while (True):
                #filling at current station
                gaz_in_tank += gaz_at_current_station[current_visiting_station]

                #can we move to the next station?
                if gaz_in_tank < cost_to_next_station[current_visiting_station]:
                    break
                else:
                    #go to the next station
                    gaz_in_tank -= cost_to_next_station[current_visiting_station]

                    # handling circular buffer
                    if current_visiting_station == nb_gaz_station - 1:
                        current_visiting_station = 0
                    else:
                        current_visiting_station += 1

                #have we made a complete loop?
                if current_visiting_station == i:
                    return i


        return -1


sol = Solution()
out = sol.canCompleteCircuit(A,B)
print(out)


def minCut(A):
    nb_cuts = 0

    if A == A[::-1] or not A:
        return 0

    if len(A) == 2:
        return 1

    # aaababaaaX
    idx = len(A) - 2
    while A and idx != -1:
        current = A[idx:]
        # print({current})
        if current == current[::-1]:
            nb_cuts += 1
            A = A[0:idx]
            idx = len(A) -2

        else:
            idx -= 1

    return nb_cuts

'''



# @return an integer

