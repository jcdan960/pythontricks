
nb_operation = 0

# disk 1 -> small
# disk 2 -> med
#...


def hanoi_towers(n, from_tower, to_tower, aux_tower):
    if n == 1:
        print(f'Moving disk 1 from tower {from_tower} to tower {to_tower}')

    else:
        hanoi_towers(n-1, from_tower, aux_tower, to_tower)
        print(f'Moving disk {n} from tower {from_tower} to tower {to_tower}')

        hanoi_towers(n-1,aux_tower,to_tower,from_tower)


#n is the number of disks to be moved
hanoi_towers(3, 'A', 'C', 'B')
print(f'nb operations: {nb_operation}')