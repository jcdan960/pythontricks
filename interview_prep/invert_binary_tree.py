# Definition for a  binary tree node
class TreeNode:
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

	def add_branch(self, left, right):
		self.left = left
		self.right = right

class Solution:
	# @param A : root node of tree
	# @return the root node in the tree
	def invertTree(self, A):
		if A.left:
			A.left.val, A.right.val = A.right.val, A.left.val
			return self.invertTree(A.val)

		else:
			return A


sol = Solution()
tree = TreeNode(1)

'''
	 1
   /   \
  3     2
 / \   / \
7   6 5   4

'''
