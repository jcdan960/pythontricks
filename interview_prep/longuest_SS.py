# Function to find the length of the longest common subsequence of
# sequences `X[0…m-1]` and `Y[0…n-1]`

# if last char is same in X and Y, then LCS(X[1…m-1], Y[1…n-1]) + X[m]
# else max_len(LCS(X[1…m-1], Y[1…n])   ,  LCS(X[1…m], Y[1…n-1]))

'''
ex:
X: ABCBDAB (n elements)
Y: BDCABA (m elements)

does not finish with same letter

Case 1: If LCS ends with B, then it cannot end with A, and we can remove A from the sequence Y, and the problem reduces to LCS(X[1…m], Y[1…n-1]).
Case 1: If LCS doest not with B, then we can remove B from X and prob is now LCS(X[1…m-1], Y[1…n])

LCS(ABCBDAB, BDCABA) = maximum (LCS(ABCBDA, BDCABA), LCS(ABCBDAB, BDCAB))

where

LCS(ABCBDA, BDCABA) = LCS(ABCBD, BDCAB) + A
LCS(ABCBDAB, BDCAB) = LCS(ABCBDA, BDCA) + B

'''


def LCSLength(X, Y, m, n):

    if m == 0 or n == 0:
        return 0

    if X[m-1] == Y[n-1]:
        return LCSLength(X[:-1], Y[:-1] ,m-1, n-1) + 1

    else:
        return max(LCSLength(X[:-1], Y, m-1, n), LCSLength(X, Y[:-1], m, n-1))

if __name__ == '__main__':


    X = "ABCBDAB"
    Y = "BDCABA"

    #LCS are BDAB, BCAB, and BCBA

    print("The length of the LCS is", LCSLength(X, Y, len(X), len(Y)))
