#https://leetcode.com/problems/house-robber/
from typing import List

class Solution:
    def rob(self, nums: List[int]) -> int:

        prev_max, current_max = 0,0
        for i in nums:
            temp = max(i+prev_max, current_max)
            prev_max = current_max
            current_max = temp

        return current_max

        '''
        prev_max_rob, max_rob = 0,0

        for n in nums:
            current_max = max(n + prev_max_rob, max_rob)
            prev_max_rob = max_rob
            max_rob = current_max

        return max_rob
'''

        '''

    def rob(self, nums: List[int]) -> int:
        total : int = 0

        if len(nums) <= 3:
            return max(nums)

        largest_index = 0
        current_max = -1000
        for i in range(0, len(nums)):
            if i == 0:
                if nums[i] - nums[i-1] > current_max:
                    current_max = nums[i] - nums[i-1]
                    largest_index = i

            elif i == len(nums) -1 :
                if nums[-1] - nums[-2] > current_max:
                    current_max = nums[-1] - nums[-2]
                    largest_index = i

            elif nums[i] - nums[i-1] - nums[i+1] > current_max:
                current_max = nums[i] - nums[i-1] - nums[i+1]
                largest_index = i

        net = nums[largest_index] - nums[largest_index-1] - nums[largest_index+1]
        nums.pop(largest_index)
        nums.pop(largest_index-1)
        nums.pop(largest_index+1)

        return self.rob(nums) + net


'''

sol = Solution()
nums = [1,2,3,1]
my_sol = sol.rob(nums)

print(my_sol)