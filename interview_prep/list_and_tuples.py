def new_ds(ds_name):
    print(f'\n\nNEW DATASTRUCTURE : {ds_name.upper()}\n')


def new_exercise(exo):
    print(f'\n\nNEW EXERCISE : {exo.upper()}\n')

# passed: int, float, bool ?
# to do: list, tuples, dict, set
# to do: str, json

#to do advanced: https://www.educative.io/blog/8-python-data-structures#arrays

new_ds('list')
#mutable

cars = ["Toyota", "Tesla", "Hyundai"]
print(cars)

# append
cars.append('Honda')
cars.append('Honda')
cars.append('Honda')
cars.append('Honda')

print(cars)

#remove the last one like a stack
cars.pop()
print(cars)


nb_honda = cars.count('Honda')
print(nb_honda)

#remove at index 2
print(cars)
cars.pop(2)
print(cars)

cars2 = ['Tesla', 'Toyota']

common = [i for i in cars if i in cars2]
print(common)

cars.extend(cars2)
print(cars)


#first occurence of toyota
x = cars.index('Honda')
print(x)

cars.pop()
print(cars)
cars.reverse()
print(cars)

#insert at index, will move the rest afterward
cars.insert(3, 'YO')
print(cars)

#remove by object, as opposed to by index (pop method)
print(cars)
cars.remove('Tesla')
print(cars)

#clear
cars.clear()
print(cars)


## NOTE: interface is the same between list and tuple
new_exercise('Write a Python program to sum all the items in a list')

def sum_all_list(lst):
    return sum(lst)


new_exercise('Concatenate two lists index-wise')

def concat(lst1, lst2):
    lst3 = []
    for i in range (0, len(lst1)):
        lst3.append(list1[i]+list2[i])
    return lst3

list1 = ["M", "na", "i", "Ke"]
list2 = ["y", "me", "s", "lly"]
print(concat(list1, list2))


new_exercise('Given a Python list of numbers. Turn every item of a list into its square')

aList = [1, 2, 3, 4, 5, 6, 7]
squared_list = [i**2 for i in aList]

print(squared_list)


new_exercise('Remove empty strings from the list of strings')
list1 = ["Mike", "", "Emma", "Kelly", "", "Brad"]

list2 = [i for i in list1 if i]
print(list2)


new_exercise('Given a Python list, remove all occurrence of 20 from the list')
list1 = [5, 20, 15, 20, 25, 50, 20]

while list1.count(20) != 0:
    list1.remove(20)
    print('remove a 20')

print(list1)
