from collections import OrderedDict
from queue import Queue, LifoQueue

#ordered dict

od = OrderedDict()

od['a'] = 3
od['b'] = 4
od['c'] = 69

print(od)

print('\n\nQUEUE')
#queue
# dequeue and enqueue O(1)

my_queue = Queue()
my_queue.put('A')
my_queue.put('B')

print(my_queue.qsize())

print(my_queue.empty())

first_in_queue = my_queue.get()

print(first_in_queue)

print('\nlimited queue')

limited_queue = Queue(maxsize=4)
limited_queue.put(3)
limited_queue.put(4)
limited_queue.put(123)
limited_queue.put(33)

print(f'Is queue full? : {limited_queue.full()}')



print('\n\n Stack')
#stack
# push and pop O(1), random acces O(n)

#using a list

stack = ['a', 'b', 'c']
x = stack.pop()
print(x)

stack.append('k')
print(stack)

stack = LifoQueue()
stack.put('eat')
stack.put('sleep')
stack.put('code')

print(stack.get())
print(stack.qsize())



#linkedlist  quick for insertion and deletion



#tree


#graph


#hashmaps


''' 
SOME COMPLEXITY

List
Insert: O(n)
Get Item: O(1)
Delete Item:O(n)
Iteration: O(n)
Get Length: O(1)

Dictionary
Get Item: O(1)
Set Item: O(1)
Delete Item: O(1)
Iterate Over Dictionary: O(n

Set
Check for item in set: O(1)
Difference of set A from B: O(length of A)
Intersection of set A and B: O(minimum of the length of either A or B)
Union of set A and B: O(N) with respect to length(A) + length(B)

'''