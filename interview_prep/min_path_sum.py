from typing import List

#https://leetcode.com/problems/minimum-path-sum/
class Solution:
    #O(n*m)
    def minPathSum(self, grid: List[List[int]]) -> int:
        rows = len(grid)
        cols = len(grid[0])
        res = [[float('inf')] *(cols+1) for i in range(rows+1)]
        res[rows-1][cols] = 0

        for i in range(rows -1, -1, -1):
            for j in range(cols-1 , -1, -1):
                res[i][j] = grid[i][j] + min(res[i+1][j], res[i][j+1])

        return res[0][0]


        '''
        rows = len(grid)
        cols = len(grid[0])

        min_path = grid[0][0]
        x, y = 0, 0

        while (x != rows -1 or y != cols -1):
            if grid[x+1][y] < grid[x][y+1]:
                x+=1
                min_path+=grid[x][y]
            else:
                y+=1
                min_path+=grid[x][y]

        if x != rows-1:
            #need to go to the right
            for i in range(x, rows-1):
                min_path += grid[i][-1]
        else:
            for i in range(x, cols-1):
                min_path += grid[-1][i]

        return min_path
'''

grid = [[1, 3, 1], [1, 5, 1], [4, 2, 1]]
sol = Solution()
print(sol.minPathSum(grid))
