# https://www.youtube.com/watch?v=H1FD2IObWhg&list=PLzMcBGfZo4-nhWva-6OVh1yKWHBs4o_tv&index=3&ab_channel=TechWithTim

# unpacking
# tups are const by default
print('unpacking\n')
tup = (1, 2, 3)
lst = [4, 5, 6]
dic = {"aa": 54, "bb": 68, "cc": 102}
string = "Salut"

a, b, c = tup
d, e, f = lst
g, h, i = dic  # work for Items and values
j, k, l = dic.values()  # work for Items and values

_, _, m, n, o = string

print(f"Unpacking tuple\n:{a} {b} {c}")
print(f"Unpacking list\n:{d} {e} {f}")
print(f"Unpacking dict keys\n:{g} {h} {i}")
print(f"Unpacking dict values \n:{j} {k} {l}")
print(f"Unpacking string \n:{m} {n} {o}")

print('swap\n')
# variable swap:
x, y = 5, 6
print(x)
print(y)

x, y = y, x
print(x)
print(y)

# ternary
print('ternary\n')
x = 0
y = 2 if x == 0 else 5;
print(y)

print('comprehension\n')
# comprehension list/dic/tuple
list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
x = [i for i in list2 if i % 2 == 0]
print(x)

# with in range
x = [i for i in range(0, 200)]
print(x)

# for dict
x = {char: 1 for char in string}
print(x)

# zip function, to combine together container
names = ['JC', 'Yo', 'Maggie']
ages = [23, 55, 1]
eyes_colors = ['brown', 'blue', 'green']

zipped = zip(names, ages, eyes_colors)

print(list(zipped))


# *args & **kwargs
# positional args
def func1(arg1, arg2, arg3):
    print(arg1, arg2, arg3)


def func2(arg1=None, arg2=None, arg3=None):
    print(arg1, arg2, arg3)


func1(4, 5, "yo")
func2(arg1=2, arg3="yolo", arg2=45)

kwargs = {"args2": 123, "args3": 5, "args1": 256}

args = [1, 2, 3]
# unpacking the list with the index
func1(*args)

print('With kwargs')
func2(*kwargs.values())


# class, static and str method
class Person:
    population = 50

    def __init__(self, name, age):
        self.name = name
        self.age = age

    @staticmethod
    def get_population():
        return Person.population

    def normal_function():
        print("this is just a regular function, not bound to the class")

    def Boris_creator(cls, age):
        return Person("Boris", age)

    def __str__(self):
        return "YOOOO"


'''NOTES:
Str overwrites the str method'''

me = Person("JC", 30)
print(str(me))

'''population is a static variable of the class can be accessed anywhere '''
print(Person.population)

'''But name and age are not static'''

'''get_population is static because it is decorated like it'''
print(Person.get_population())

''' classMethod is often used for factory'''

me.Boris_creator(22)

# map function
print("Map\n")

li = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def power_of_two(x):
    return x * x


power_li = [power_of_two(i) for i in li]

print(power_li)

# or with map. map run the function for each element
power_li_lambda = map(power_of_two, li)

print(list(power_li_lambda))

# filter
print('\nfilter\n')


def add7(x):
    return x + 7


def is_odd(x):
    return x % 2 != 0


a = [x for x in range(10)]

print('before filtering')
print(a)
print('after')
b = list(filter(is_odd, a))
print(b)

# filter and map function takes the same parameters
# which are functon, iteratable

'''
Map takes all objects in a list and allows you to apply a function to it.
Filter takes all objects in a list and runs that through a function to create a new list with all objects that return True in that function.
'''

# lambda function (anonymous function)
print('No yet lambda')


def cool_func(x):
    return x + 5


print(cool_func(4))

print('lambda')

# name      =     param    return val
cool_func2 = lambda x: x + 4

print(cool_func2(2))

#cool example lamda + map

a = [i for i in range(54,123)]
b = list(map(lambda x:x-55, a))
print(a)
print(b)


#************************************************
x = "a string"
print(x)

print("and the reverse of the string")
print(x[::-1])

#************************************************
#Range keyword:

print('\n\n range based stuff')

for i in range(5):
    print(i)

print('in range(5) out puts 0-4')
print('when no first number, starts a 0\n\n')


for i in range(1, 5):
    print(i)

print('in range(1-5) out puts 1-4')

#****************************************************
#Enumerate

cool_list = [x**2 for x in range(12)]
for index, val in enumerate(cool_list):
    print(f'Index {index} has value {val}')