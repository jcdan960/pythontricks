# https://www.youtube.com/watch?v=HGOBQPFzWKo&t=5803s&ab_channel=freeCodeCamp.org

def new_subject(subject):
    print('\n\n')
    print(f'****************************{subject.upper()}****************************')
    print('\n\n')


new_subject('list')
# Lists:

my_list = [i for i in range(10)]
print(my_list)

# reverse a list:
print(my_list.reverse())

print('\n')

# list can have different data type in it
second_list = ['A', 12, 5.0]
print(second_list)

# remove an element at the end: pop
second_list.pop()
print(second_list)

# add to list
second_list.append("Yo")
print(second_list)

# acces by index
x = second_list[1]
print(x)

# print the last item
print(my_list[-1])

print('\n\n')

# make a copy
copy = second_list.copy()
print(copy)

# exted a list with another list
other = ['test']
second_list.extend(other)
print(second_list)

# lenght
print(f'List lenght is {len(my_list)}')

# remove by reference
print('before removal')
print(second_list)
second_list.remove('test')
print('after remove')
print(second_list)

# clear the list
print('before clear')
print(second_list)
second_list.clear()
print('after clear')
print(second_list)

# slicing:
# slice[first:last:step]
# https://stackoverflow.com/questions/509211/understanding-slice-notation
super_list = ['A', 'B', 'C', 'D', 'E']
a = super_list[1:2]
# get index 1 to 2(excluded)
print(a)

# no index begining
b = super_list[:2]
print(b)

# with step: begining to the end with step of 2
c = super_list[::2]
print(c)

d = super_list[1:5:1]
print(d)

new_subject('dict')
# unordered and mutable
mydict = {'jc': 123, 'jess': 45, 'maggie': 2}
print(mydict)

# has the same functions
other = mydict.copy()
print(other)
other.pop('jc')
print(other)

other.clear()
print(other)

# get keys and or values:
print(mydict.values())
print(mydict.keys())
print(mydict.items())

new_subject('string')
# string

my_str = 'Salut'
a, b, c, d, e = my_str
print(a, b, c, d, e)

# upper and lower
low = my_str.lower()
print(low)

up = my_str.upper()
print(up)

# join list of string
test_list = ['This', 'is', 'a', 'Test']
test_str = ' '.join(test_list)
print(test_str)

# inverse, from string to list of string
print(test_str.split(' '))

print('\n\n')

# substring can be done with slicing
print(f'finding substring with base string \n{my_str}')
print(my_str[1:4:2])

# clean whitespace
test_str = '      JC'
print(test_str)
print(test_str.strip())
# can also use lstrip and rstrip

# other cool methods:
my_str = '12'
if my_str.isnumeric():
    print(f'{my_str} is numeric')

# other to use:
print(f'my string is alpha {my_str.isalpha()}')
print(f'my string is ascii {my_str.isascii()}')

# sub string
print('\n\n SubString')
my_str = 'Hello JC'
if 'JC' in my_str:
    print('its in')
else:
    print('nope')

# findind the index
out = my_str.find('JC')
print(f'{my_str} has str JC at index {out}')

# how many char we have
char_to_find = 'J'
print(f'We have {my_str.count(char_to_find)} in {my_str}')

# replace
new_str = my_str.replace('JC', 'Jess')
print(new_str)

new_subject('tuple')
# tuple : ordered, immutable

my_tuple = (2, 3, 'test')
print(my_tuple)

# to get last element:
print(my_tuple[-1])

# finding the index
tpl = ('a', 'b', 'c')

idx = tpl.index('c')
print(idx)

# tuple are more efficient then list

new_subject('set')
# set: mutable but no double allowed

my_set = {'a', 2, 3}
print(my_set)

# add and remove element from set
my_set.add("jc")
print(my_set)

my_set.discard("jc")
print(my_set)

# pop 1 element (last one)
print('\n\npopping')
print(my_set)
my_set.pop()
print(my_set)

# union, intersection and difference
print('\n\nunion and intersection')

even = {0, 2, 4, 6, 7}
primes = {2, 3, 5, 7}

print(f'Even:\n{even}\n\nPrimes:\n{primes}')

print(f'Union of primes and even: {primes.union(even)}')

print(f'Intersection of primes and even: {primes.intersection(even)}')

print(f'Difference of primes and even: {primes.difference(even)}')

print('Odds:\n{odds}')
odds = {1, 3, 5, 7, 9}
# merging sets:
odds.update(even)
print(odds)

# subset:
setA = {1, 2, 3, 4, 5, 6, 7}
setB = {3, 4}

print(setB.issubset(setA))
# B is a subset of A, but A not a subset of B
print(setA.issubset(setB))

# frozenset : immutable
x = frozenset()

import copy, random

# shallow copy: copy one level deep, only references of the nested child objects
# deep copy: everyting is copied
new_subject('copying')
org = 5
# this is a ref
ref = org

org = [1, 2, 3, 4, 5]
ref = org

# shallow copy
cpy = copy.copy(org)
org.append(123)

print(org)
print(cpy)

# nested list, need deep copy
lst = [random.randint(1, 6) for i in range(3)]
nested = [lst for x in range(2)]
print(nested)

# shallow copy:
shallow = copy.copy(nested)
# changing the original
nested[1][1] = 123123
print(nested)
print(shallow)

# deep copy:
deep = copy.deepcopy(nested)
print(nested)
print(deep)

new_subject('errors handling')
# python program stop when run in an exception or syntax error
# syntax error: e.g.  x = ((5)

# exeptions:
# https://www.tutorialspoint.com/python/standard_exceptions.htm
# TypeError, IndentationError, IOError, NameError, IndexError, ZeroDivisionError, FileNotFoundError
# base class for exception is Exception

# forcing an exepcetion:
x = 4
try:
    if x < 5:
        raise Exception("Should not be less than 5")
except Exception as e:
    print(f'exception catched: {e}')

finally:
    print("finally everything is fine")

# asserting:

assert (x <= 4)  # ok


class VeryBadError(Exception):
    def __init__(self):
        return


new_subject('JSON')
import json, os

# convert dict to json
person = {"name": "John", "age": 30, "city": "New York", "hasChildren": False, "titles": ["engineer", "programmer"]}
person_json = json.dumps(person)
print(person_json)

person_json_indent = json.dumps(person, indent=4)
print(person_json_indent)

# save to file (serialisation)
base = os.getcwd()
path = os.path.join(base, 'person.json')
print(path)
with open(path, 'w+') as f:
    json.dump(person, f)
    f.close()

# deserialisation
with open('/home/jc/Documents/repo/pythontricks/example.json', 'r') as f:
    person_json_loaded = json.load(f)
    print(person_json_loaded)

# jsonifying
print('\njsonifying')


class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def to_dict(self):
        return {'name': self.name, 'age': self.age}


jc = User('JC', 30)
jcJson = json.dumps(jc.to_dict())
print(jcJson)

new_subject('random numbers')
# for "real" random number, user the secret module
import secrets

a = secrets.randbelow(10)
print(a)

new_subject('decorators')


# function and class decorators

# a decorator is a function that takes another functions as argument -> add functionalities
# function can take function as arg, and also return a function
def my_decorator(func):
    def wrapper():
        print("inside wrapper")
        func()
        print('finishing wrapper')

    return wrapper


@my_decorator
def do_stuff():
    print('doing stuff you know...')


do_stuff()

# decorator with arguments:

print(f'\nSECOND DECORATOR with arguments')


def second_decorator(func):
    def wrapper(*args, **kwargs):
        print("second decorator starting")
        result = func(*args, **kwargs)
        print("second decorator ending")
        return result

    return wrapper


@second_decorator
def second_function(x):
    print(x + 5)
    return 123


res = second_function(7)
print(res)

# Class decorator:
print('Class decorateor')


class CallCounter:
    def __init__(self, func):
        self.func = func
        self.num_calls = 0

    def __call__(self, *args, **kwargs):
        self.num_calls += 1
        print(f'from the call function, executed {self.num_calls} times')
        if self.func is not None:
            return self.func(*args, **kwargs)


@CallCounter
def say_hello():
    print("Hello")

# how many time
counter = CallCounter(say_hello)
say_hello()
say_hello()

new_subject('misc')


class SuperJC:
    def __init__(self):
        return


def test():
    return


print(f'getting a function name:{test.__name__}')
print(f'getting a class name:{SuperJC.__name__}')

new_subject('generators')

#function that return an object that can be iterated over
#defined with the yield keyword instead of the return keyword

def my_generator():
    yield 1
    yield 2
    yield 3

g = my_generator()
val = next(g)
print(f'val: {val}')

for i in g:
    print(i)

print('\n\n\n')

def countdown(num):
    print('Starting')
    while num > 0:
        yield num
        num -= 1

cd = countdown(4)

for i in cd:
    print(i)

#generator are memory efficient and fast!

#bad way:
def firstn(n):
    nums = []
    num = 0
    while num<n:
        nums.append(num)
        num +=1
    return nums


#good way:
def firstn_generator(n):
    num = 0
    while num < n:
        yield n
        num += 1


import sys
print('\n\n')
print(sys.getsizeof(firstn(1000)))
print(sys.getsizeof(firstn_generator(1000)))


#in 1 line we can create a generator similar to list comprehension

my_gen = (i for i in range(10000) if i%2 == 0)
my_list = [i for i in range(10000) if i%2 == 0]

print(f'Generator: {my_gen}\n with size {sys.getsizeof(my_gen)}')
print(f'List: {my_list}\n with size {sys.getsizeof(my_list)}')

