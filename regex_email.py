import re

#with the help of : https://regex101.com/
'''

It must have the username@websitename.extension format type.
The username can only contain letters, digits, dashes and underscores .
The website name can only have letters and digits .
The extension can only contain letters .
The maximum length of the extension is .
'''


# any alphanumeric or _ or - --- @ --- any alphanumeric -- . any alpha, less than 4 char
def mail_is_valid(email):
    if not email or type(email) != str:
        return False

    username = r'@'
    username_regex = re.compile(username, re.IGNORECASE)

    username_search = username_regex.split(email)
    if not username_search or len(username_search) != 2:
        return False

    username_found = username_search[0]

    #begining of a string, alpha numeric lower or upper. 1 or more
    alpha_numeric_plus = '^[a-zA-Z0-9_\-\.]+$'
    if not re.match(alpha_numeric_plus, username_found):
        return False

    rest = username_search[1]
    domain = '\.'
    domain_regex = re.compile(domain, re.IGNORECASE)

    splitted_rest = domain_regex.split(rest)
    if not splitted_rest or len(splitted_rest) != 2:
        return False

    domaine_name = splitted_rest[0]
    extension = splitted_rest[1]
    if not domaine_name.isalnum():
        return False

    if not extension:
        return False

    if len(extension) > 3 or not extension.isalpha():
        return False

    return True



mail_is_valid('learnpoint@learningpoint.net')

answers = {None:False , 3: False, 'lara@hackerrank.com': True, 'brian-23@hackerrank.com': True, 'britts_54@hackerrank.com': True,
         '@gmail.com': False, 'xxx@jcjc.dsds': False, 'xxx@jcjc.dsds': False, '@@@': False}

for val, key in answers.items():
    if mail_is_valid(val) == key:
        print('nice')
    else:
        raise ValueError()

print('succcess')
