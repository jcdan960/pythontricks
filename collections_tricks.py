import collections

'''
CONTAINERS:

List
set (no duplicate)
dict
tuple (const)

TYPES:
counter 
deque
namedTuple
orderedDict 
defaultDict
'''

#counter just counting the instance of each:
counter = collections.Counter('abcdfaaa')
counter2 = collections.Counter([1, 2, 3, 4, 5, 56, 6, 7, 8, 9, 2, 2 ,2])
print(counter)
print(counter2)

#find the 2 most common elements
print(counter.most_common(2))

'''
deque = collections.deque
named_tuple = collections.named_tuple
ordered_dict = collections.ordered_dict
default_dict = collections.default_dict
'''
